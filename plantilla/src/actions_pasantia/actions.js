import { ID_NOMBRE_DEPARTAMENTO } from './actionTypes'

export const id_nombre_departamento=id=>({
    type: ID_NOMBRE_DEPARTAMENTO,
    payload: id
})