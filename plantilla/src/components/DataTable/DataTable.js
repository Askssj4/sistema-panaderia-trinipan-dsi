import { Row, Col } from "reactstrap"
//import { MDBDataTable } from "mdbreact"
import React, {Fragment, useState, useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import ToolkitProvider, { Search }  from 'react-bootstrap-table2-toolkit';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator'; 
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import './css/DataTable.css';

const { SearchBar } = Search;

const DataTable = props => {
  //Data que recibirá el mdbDataTable para formar sus filas y columnas

  const[ datos, setDatos]=useState([]);
  const [ columnas_tabla, setColumnas_tabla ] = useState([{dataField:"", text:""}]);


    useEffect(()=>{

      _setEstado();
    },[]);

    useEffect(()=>{

      _setEstado();
   },[props.datosTabla]);


    useEffect(()=>{

       _setEstado();
    },[props.columnasTabla]);

    const _setEstado=async()=>{
      // console.log("datos tabla en table", props.datosTabla);
      // console.log("columnas en tabla", props.columnasTabla);
      if(props.datosTabla != undefined && props.datosTabla != datos)
      {
        await setDatos(props.datosTabla);
      }

      if(props.columnasTabla != undefined && props.columnasTabla != columnas_tabla)
      {
        await setColumnas_tabla(props.columnasTabla);
      }
    }
    // console.log("render en dataTable", datos);
  return (

    <Fragment>
      <Row>
        <Col>
      <ToolkitProvider
        keyField="id"
        data={datos}
        columns={columnas_tabla}
        search
      >
        {props => (
          <>
          <div style={{
            float:"right"
          }}>
            <SearchBar
              {...props.searchProps}
              placeholder="Buscar"
              style={{
                float:"right",
                margin: "1%",
                borderRadius: "1000px",
              }}
            />
            </div>
            <div>
            <br />
            <BootstrapTable
              striped
              pagination={paginationFactory({
                withFirstAndLast: false,
              })}
              {...props.baseProps}
            />
          </div>
          </>
        )}
      </ToolkitProvider>
      </Col>
      </Row>
    </Fragment>
  )
}

export default DataTable
